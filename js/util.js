const $phone = $('input[name=Phone]');
const $name = $('input[name=Name]');
const $address = $('input[name=Address]');
const $product = $('input[name=Product]');
const $color = $('select[name=Color]');
const $message = $('textarea');
const $modal = $('.modal');
$('.modal-footer .btn-primary').on('click', function () {
  $.ajax({
    url:
      'https://docs.google.com/forms/u/0/d/e/1FAIpQLSctsn9H3n6ndQIXS5ehrl9JS_WtUoxWtArIbLixyS2Sxj-PXw/formResponse',
    data: {
      'entry.1037927208': $phone.val(),
      'entry.1445574705': $name.val(),
      'entry.1553306132': $address.val(),
      'entry.1759236834': $product.attr('placeholder'),
      'entry.991636165': $color.val(),
      'entry.1301850914': $message.val(),
    },
    type: 'POST',
    dataType: 'xml',
  })
    .done((res) => {
        $modal.modal('hide')
    })
    .fail((err) => {
        $modal.modal('hide')
    });
});
